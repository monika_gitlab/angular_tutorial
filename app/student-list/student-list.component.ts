import { Component, OnInit } from '@angular/core';
import { Student } from '../student';
import { STUDENTS } from '../mock_students';
import { StudentService } from '../student.service';

@Component({
  selector: 'app-student-list',
  templateUrl: './student-list.component.html',
  styleUrls: ['./student-list.component.css']
})
export class StudentListComponent implements OnInit {
	
	students: Student[]=[];
  constructor(private studentService:StudentService) { }
  
  
  
  // getStudents():void{
	//   this.students = this.studentService.getStudents();
  // }//============synchronous=========

  getStudents():void{
    this.studentService.getStudents().subscribe(students => this.students =students);
  }
  
  selectedStudent : Student;
  onSelect(student:Student){
	  this.selectedStudent = student;
  }
  ngOnInit() {
	  this.getStudents();
  }

}
