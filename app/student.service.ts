import { Injectable } from '@angular/core';
import { Student } from './student';
import { STUDENTS } from './mock_students';

import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StudentService {
  
  /* students:Student[] = [
  {id:1,name:"Rajvardhan"},
  {id:2,name:"Ishwari"},
  {id:3,name:"Saee"},
  {id:4,name:"Adi"},
  {id:5,name:"Raghu"},
  {id:6,name:"Anirudhha"},
  {id:7,name:"Pauras"}
  ];
   */
  
  getStudents():Observable<Student[]>{
	  return of(STUDENTS);
  }
  
  getStudent(id: number):Observable<Student>{
	  
		return of(STUDENTS.find(student => student.id === id));
  }
  
  constructor() { }
}
