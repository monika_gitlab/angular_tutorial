import { Component } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html' /*
  `<input type="text" bind-id="myId" value="Monika">
  <button (click)="onClick()">Display</button>
  <input type="text" [(ngModel)]="myId">{{myId}}
  ` */,
  styleUrls: ['./app.component.css']
})
export class AppComponent {
	
	title = 'Student App';
	
	/*--------reactive form 2-------------
	submitted = false;
	constructor(private fb:FormBuilder){}
	validateForm = this.fb.group({
		fname:['',Validators.required],
		lname:['',Validators.required],
		pinCode:['',[Validators.required,Validators.minLength(6)]]
	});
	get formElements(){return this.validateForm.controls;}
	onSubmit(){
		this.submitted = true;
		if(this.validateForm.valid){
		//console.log(this.insertForm.value);//reactive form 1
		console.log(this.validateForm.value);//reactive form 2
		}
		else { return;}
		this.submitted = false;
		this.validateForm.reset();
	}
	------------------Reactive form 2------------------*/
	/* insertForm = new FormGroup({
		fname : new FormControl(''),
		lname : new FormControl('')
	}); */
		
	/*----------------Template-driven form---------
	title='Template-driven Form';
	TRVName;
	name;
	lname;
	getData(nm:any){
		this.TRVName = nm;
		this.name = this.TRVName.txtfname;
		this.lname = this.TRVName.txtlname;
		console.log(this.name);
		console.log(this.lname);
	}-----------------Template-driven form---------*/
	/*public myId=123;
	innerText = 'Practice';
	onClick(){
		console.log('Welcome to Angular');
	}
	/*------------*1* FOR TWO WAY Data binding without ngModel 
	
  updateValue(e){
	  console.log(e.target.value);
	  this.innerText = e.target.value;
  }----------------FOR TWO WAY Data binding without ngModel---*1*----*/
  
  /*---*2*-----------------------*ngFor--------------------
  Sisters=[
  {
	  name:'Reshma',
	  IsOnline:'False'
  },
  {
		name:'Rohini',
		IsOnline:'true'
  },
  {
		name:'Manisha',
	  IsOnline:'true'
  },
  {
	  name:'Monika',
	  IsOnline:'true'
  }
  ]
  -------------------------------------*ngFor-----*2*-----*/
  
  /*--*3*--------------component communication----------------
  public name="Monika";
  public message="";
  /*----------------component communication-------------*3*---*/
  
  /*--*4*------------------pipes--------------------------*
   message:string="hello Monika";
   data={
	   name:"Radha",
	   Mobile:"8888888888"
   }
  /*--*4*------------------pipes--------------------------*/
}
