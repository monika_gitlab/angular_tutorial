import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.css']
})
export class ChildComponent implements OnInit {
	/*----------------------component communication*/
	@Input() public parentData;
	@Input('parentData') public nameC; //nameC is alias name.parent to child
	
	@Output() public childEvent=new EventEmitter();
	/*--component communication------------------*/
	
  constructor() { }

  ngOnInit() {
  }
  /*-------------component communication-----------*/
  fireEvent(){
	  this.childEvent.emit('hey!! this is event from child to parent');
  }
/*----------------component communication-----------------*/
}
