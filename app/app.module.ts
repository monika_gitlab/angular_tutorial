import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';//for ngModel two way data binding //TDForm

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ChildComponent } from './child/child.component';//i have added it for component communication
import { ExponentialStrenthPipe } from './exponential-strenth.pipe';
import { ContactComponent } from './contact/contact.component';
import { AddressComponent } from './contact/address/address.component';
import { StudentListComponent } from './student-list/student-list.component';
import { StudentDetailsComponent } from './student-details/student-details.component';

@NgModule({
  declarations: [
    AppComponent,
    ChildComponent,
    ExponentialStrenthPipe,
    ContactComponent,
    AddressComponent,
    StudentListComponent,
    StudentDetailsComponent
  ],
  imports: [
    BrowserModule,
	FormsModule,//for ngModel
	ReactiveFormsModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
