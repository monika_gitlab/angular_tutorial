import { Student } from './student';

export const STUDENTS : Student[] = [
  {id:1,name:"Rajvardhan"},
  {id:2,name:"Ishwari"},
  {id:3,name:"Saee"},
  {id:4,name:"Adi"},
  {id:5,name:"Raghu"},
  {id:6,name:"Anirudhha"},
  {id:7,name:"Pauras"}
  ];
  